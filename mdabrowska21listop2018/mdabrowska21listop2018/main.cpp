//
//  main.cpp
//  mdabrowska21listop2018
//
//  Created by macbook_monika on 21/11/2018.
//  Copyright © 2018 macbook_monika. All rights reserved.
//

#include <iostream>

class publikacja
{
private:
    int rok_wydania;
    
public:
    publikacja(int);
    virtual void drukuj();
    int rokWydania();
};

publikacja::publikacja(int n)
{
    rok_wydania = n;
}
void publikacja::drukuj()
{
    std::cout << "Rok wydania: " << rok_wydania << std::endl;
}

int publikacja::rokWydania()
{
    return rok_wydania;
}
class ksiazka : public publikacja
{
private:
    int cena;
    
public:
    ksiazka(int, int);
    void drukuj();
};

ksiazka::ksiazka(int rok_wyd, int n) : publikacja(rok_wyd)
{
    cena = n;
}
void ksiazka::drukuj()
{
    std::cout << "Rok wydania: " << rokWydania() << std::endl;
    std::cout << "Cena: " << cena << std::endl;
}

int main(int argc, const char *argv[])
{
    
    int r1;
    int r2;
    int c;
    std::cout << "Podaj rok 1: ";
    std::cin >> r1;
    std::cout << "Podaj rok 2: ";
    std::cin >> r2;
    std::cout << "Podaj cene: ";
    std::cin >> c;
    publikacja obiekt1(r1);
    ksiazka obiekt2(r2, c);
    
    publikacja *p1 = &obiekt1;
    
    p1->drukuj();
    p1 = &obiekt2;
    p1->drukuj();
    return 0;
}
