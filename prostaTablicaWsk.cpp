/*
 
 UML
 
 klasa_tab - dynamiczna alokacja pamięci
 
 - tab: int []
 
 + klasa_tab
 + drukuj(): void
 + szukaj(n: int) : bool
 
*/


#include <iostream>
#include <cassert>

class klasa_tab{ //deklaracja klasy, pól i metod
     
private:
	int *tab1;

public: // klasa pochodna ma dostęp do publicznej częsci klasy
	klasa_tab(int *);
	~klasa_tab();
    void drukuj();
    bool szukaj(int);
};

klasa_tab::klasa_tab(int t[]){ // definiowanie
	
	tab1= new int [9];
	assert(tab1);
	
	for(int i=0; i<9; i++){
        tab1[i] = t[i];
        std::cout<<"konstruktor"<<std::endl;
    }
}

void klasa_tab::drukuj(){
    
    for(int i=0; i<9; i++){
        
        std::cout << tab1[i] << " " << std::endl;
    }
}

bool klasa_tab::szukaj(int n){
    
    bool tmp = 0;
    for(int i = 0; i<9; i++){
        if(tab1[i] == n){
            tmp = true;
            i = 9;
        }
    }
    return tmp;
}
klasa_tab::~klasa_tab(){
	delete[]tab1;
}


int main(void){
    
    /*
     std::cout <<
     std::cin >>
     std::cout <<  << std::endl;
     */
    
    int tab[10];
    std::cout << "Podaj wartości tablicy " << std::endl;
    for(int i = 0; i < 9; i++){
        std::cin >> tab[i];
    }
    
    klasa_tab obiekt1(tab);
    
    obiekt1.drukuj();
    std::cout << "Odp: " << obiekt1.szukaj(5);
    obiekt1.~klasa_tab();
    return 0;
}

