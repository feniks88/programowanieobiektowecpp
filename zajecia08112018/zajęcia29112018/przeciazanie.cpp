#include <iostream>

class zespolone
{
  private:
    int re, im;

  public:
    zespolone(int, int);
    void drukuj();
    zespolone operator+(zespolone);
    zespolone operator-(zespolone);
    friend std::ostream &operator<<(std::ostream &, zespolone); //friend std to osobna klasa
    friend std::istream &operator>>(std::istrem &, zespolone);
}

zespolone::zespolone(int i, int j)
{
    re = i;
    im = j;
}
void zespolone::drukuj()
{
    std::cout << re << " + " << im << "*i" << std::endl;
}

zespolone zespolone::operator+(zespolone z0)
{
    int rz, ur;
    rz = re + z0.re;
    ur = im + z0.im;
    return zespolone(rz, ur)
}

zespolone zespolone::operator-(zespolone z0)
{
    int rz, ur;
    rz = re - z0.re;
    ur = im - z0.im;
    return zespolone(rz, ur)
}

std::ostream &operator<<(std::ostream &wyjscie, zespolone z1)
{
    wyjscie << "(" << z1.re << ", " << z1.im << ")" << std::endl;
    return wyjscie;
}

std::istream &operator>>(std::istream &wejscie, zespolone &z1)
{
    std::cout << "Re? ";
    wejscie >> z1.Re;
    std::cout << "Im? ";
    wejscie >> z1.Im;
    return wejscie;
}

int main()
{
    zespolone z1(1, 2), z2(4, 10), z3(0, 0), z4(0, 0);
    z3 = z1 + z2;          // przeciązenie
    z4 = z1.operator-(z2); //przeciazenie
    std::cin >> z1;
    std::cout << z1;
}