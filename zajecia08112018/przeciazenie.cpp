#include <iostream>

class zespolone
{
    int re, im;

  public:
    zespolone(int, int);
    zespolone operator+(zespolone);
    zespolone operator-(zespolone);
    void drukuj();
    friend std::ostream &operator<<(std::ostream &, zespolone);
    //friend std::istream &operator>>(std::istream &, zespolone);
};
zespolone::zespolone(int i, int j)
{
    re = i;
    im = j;
}
void zespolone::drukuj()
{
    std::cout << re << " " << im << std::endl;
}
zespolone zespolone::operator+(zespolone z0)
{
    int rz, ur;
    rz = re + z0.re;
    ur = im + z0.im;
    return zespolone(rz, ur);
}
zespolone zespolone::operator-(zespolone z0)
{
    int rz, ur;
    rz = re - z0.re;
    ur = im - z0.im;
    return zespolone(rz, ur);
}
std::ostream &operator<<(std::ostream &wyjscie, zespolone z1)
{
    wyjscie << "(" << z1.re << ", " << z1.im << ")" << std::endl;
    return wyjscie;
}
/*std::istream &operator>>(std::ostream &wyjscie, zespolone z1)
{
    wejscie >> z1.re;
    wejscie >> z1.im;
    return wyjscie
}*/
int main()
{

    zespolone z1(1, 2), z2(4, 10), z3(0, 0), z4(0, 0);
    z3 = z1 + z2;          // przeciązenie
    z4 = z1.operator-(z2); //przeciazenie
    std::cout << z4;
    /* z4.drukuj();

    zespolone pierwsza(0, 0);

    std::cout << "Prosze podac czesc rzeczywista liczby zespolone"
              << "i nacisnac ENTER" << std::endl;
    std::cin >> pierwsza;
    std::cout << "Prosze podac czesc urojona liczby zespolonej"
              << "i nacisnac ENTER" << std::endl;
    std::cin >> pierwsza;
    std::cout << "podano liczbe zespolona" << pierwsza;
    */
    return 0;
}