//
//  main.cpp
//  zajecia08112018
//
//  Created by macbook_monika on 08/11/2018.
//  Copyright © 2018 macbook_monika. All rights reserved.
//

#include <iostream>
class CzastkaEl //klasa bazowa (podstawowa)
{
    double masa;
public:
    CzastkaEl(double);
    void drukuj( );
    double zwrocMase( );
};
class CzastkaElN: public CzastkaEl //klasa pochodna (dziedziczaca)
{
    float ladunekElektr;
public:
    CzastkaElN(double, float);
    void drukuj2( );
};

//metody dla klasy bazowej
CzastkaEl::CzastkaEl(double m)
{
    masa = m;
}
void CzastkaEl::drukuj( )
{
    std::cout << "masa czastki: "
    << masa << std::endl;
}
double CzastkaEl::zwrocMase( )
{
    return masa;
}

CzastkaElN::CzastkaElN(double m1,float lad2):CzastkaEl(m1),ladunekElektr(lad2){ }
void CzastkaElN::drukuj2( )
{
    std::cout << "masa czastki: " << zwrocMase( )
    << "ladunek elektryczny czastki: "
    << ladunekElektr << std::endl;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";
    return 0;
}
