#include <iostream>
#include <iomanip>
using namespace std;

class Publikacja
{

  private:
    int rok_wydania;

  public:
    Publikacja(int a) : rok_wydania(a){};
    int getYear() { return rok_wydania; };
    !~Publikacja();
    !Publikacja & operator=(Publikacja());
};
//co dziedziczy? v
//getYear() i rok_wydania.
class Ksiazka
    : public Publikacja
{