#include <iostream>

class klasa1{ //deklaracja klasy, pól i metod
	
	int n1, n2;
	
	public: // klasa pochodna ma dostęp do publicznej częsci klasy
		klasa1(int, int); //konstruktor klasy, niczego nie zwraca, nie piszemy void
		klasa1(); // konstruktor bezparamterowy, przeciążenie konstruktora
		void drukuj(); // metody klasy
		void sortowanie();
};
	klasa1::klasa1(int a, int b){ // definiowanie
		n1 = a;
		n2 = b;
	}
	klasa1::klasa1(){ // definiowanie
		n1 = 0;
		n2 = 0;
	}
	
	void klasa1::drukuj(){
		std::cout << "n1= " << n1 << ", n2= " << n2 << std::endl;
	}
	
	void klasa1::sortowanie(){
		if(n2 < n1){
			int tmp = n1;
			n1 = n2;
			n2 = tmp;
		}
	}

int main(void){

/*	
	std::cout << 
	std::cin >>  
	std::cout <<  << std::endl;
*/
	
	klasa1 obiekt1(1,2); // automatycznie wywoływany konstruktor klasy z parametrami
	obiekt1.drukuj();
	klasa1 obiekt2; // wywołanie konstruktora bezparametrowego
	std::cout << "Obiekt drugi: " << std::endl;
	obiekt2.drukuj();
	std::cout << "Obiekt trzeci: " << std::endl;
	klasa1 obiekt3(4,2);
	obiekt3.sortowanie();
	obiekt3.drukuj();
	
	return 0;
}


