#include <iostream>
#include <cassert>
#include <cstring>

class srodek_transportu
{
  private:
    char *model;

  public:
    srodek_transportu(char *);
    virtual void drukuj();
    ~srodek_transportu();
};

srodek_transportu::srodek_transportu(char *napis)
{
    int dlugosc_napisu = strlen(napis);
    model = new char[dlugosc_napisu + 1];
    assert(model);
    std::strcpy(model, napis);
}

srodek_transportu::~srodek_transportu()
{
    std::cout << "Usunieto srodek transportu" << std::endl;
    delete[] model;
}

void srodek_transportu::drukuj()
{
    std::cout << "Srodek transportu typ to: " << model << std::endl;
}

class samochod : public srodek_transportu
{
  private:
    char *paliwo;

  public:
    samochod(char *);
    void drukuj();
    ~samochod();
};
samochod::samochod(char *napis, char *napis2) : srodek_transportu(napis2)
{
    int dlugosc_napisu = strlen(napis);
    paliwo = new char[dlugosc_napisu + 1];
    assert(paliwo);
    strcpy(paliwo, napis);
}

void samochod::drukuj()
{
    drukuj();
    std::cout << "Paliwo w samochodzie to: " << paliwo << std::endl;
}

samochod::~samochod()
{
    std::cout << "Usunieto samochod" << std::endl;
    delete[] paliwo;
}

int main()
{
    srodek_transportu obiekt1("obiekt1");
    obiekt1.drukuj();
}