#include <iostream>
#include <cassert>
#include <cstring>

class samochod
{
  private:
    char *w;
    static int ilosc_obiektow;

  public:
    samochod();
    samochod(char[]);
    samochod(const samochod &);
    ~samochod();
    void drukuj();
};

int samochod::ilosc_obiektow = 0;

samochod::samochod()
{
    w = new char[strlen("jakis") + 1];
    assert(w);
    strcpy(w, "jakis");
    ilosc_obiektow++;
}

samochod::samochod(char *tekst)
{
    w = new char[strlen(tekst) + 1];
    assert(w);
    strcpy(w, tekst);
    ilosc_obiektow++;
}

samochod::samochod(const samochod &wzor)
{
    w = new char[strlen(wzor.w) + 1];
    assert(w);
    strcpy(w, wzor.w);
    ilosc_obiektow++;
}

samochod::~samochod()
{
    delete[] w;
    ilosc_obiektow--;
}

void samochod::drukuj()
{
    std::cout << "Nazwa samochodu:\t" << w << std::endl;
    std::cout << "Ilosc obiektow:\t" << ilosc_obiektow << std::endl;
}

int main()
{
    samochod obiekt1;
    std::cout << "\t\tObiekt 1:" << std::endl;
    obiekt1.drukuj();

    samochod obiekt2("aaaa");
    std::cout << "\t\tObiekt 2:" << std::endl;
    obiekt2.drukuj();

    std::cout << "\t\tObiekt 3:" << std::endl;
    samochod obiekt3 = obiekt2;
    obiekt3.drukuj();
    return 0;
}
