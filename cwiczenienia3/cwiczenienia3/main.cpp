//
//  main.cpp
//  cwiczenienia3
//
//  Created by macbook_monika on 25/10/2018.
//  Copyright © 2018 macbook_monika. All rights reserved.
//Tablica przechowująca napis

#include <iostream>

class Tablica{
private:
    char * tablica;
    int n;
public:
    Tablica(int n, char * tab);
    Tablica(const Tablica &tab);
    
};
Tablica::Tablica(int size, char *tab){
    n = size;
    tablica = new char[size];
    for(int i = 0; i<n;i++){
        tablica[i] = tab[i];
    }
}
Tablica::Tablica(const Tablica &tab){
    n = tab.n;
    tablica = new char [n];
    for(int i = 0; i<n; i++){
        tablica[i]=tab.tablica[i];
    }
}

int main(int argc, const char * argv[]) {
    // insert code here...
    int n = 5;
    char *tablica;
    char wyraz;
    tablica = new char[n];
    for(int i = 0; i < n; i++){
        std::cin >> wyraz ;
        tablica[i] = wyraz;
    }
    Tablica przyklad1(n, tablica);
    
    return 0;
}


