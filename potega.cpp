#include <iostream>

void potega(float *);
void potega_ref(float &);

int main(void){
	
	float a;
	float b;
	
	std::cout << "Podaj liczbe: " << std::endl;	
	std::cin >> a; 
	potega(&a);
	std::cout << a << std::endl;
	
	std::cout << "Podaj liczbe: " << std::endl;
	std::cin >> b; 	
	potega_ref(b);	
	std::cout << b << std::endl;
	
	zamianaLiczbWsk(&a, &b);
	std::cout << a << std::endl;
	std::cout << b << std::endl;
	
	zamianaLiczbRef(a,b);
	
	
	return 0;
}

void potega(float *a){
	
	*a = *a * *a * *a * *a;	
}

void potega_ref (float &b){ //skutek taki sam jak przez wyłuskanie, zaleca się używanie zamiast wskaźników
	
	b = b*b*b*b;
} 
