//  Zadanie domowe nr 1
//  Monika Dąbrowska - Informatyka, II rok stacjonarne

#include <iostream>

class kwadrat{
private:
    float bok;
public:
    kwadrat(float);
    float pole_powierzchni();
};
kwadrat::kwadrat(float a){
    bok = a;
}
float kwadrat::pole_powierzchni(){
    return bok*bok;
}
int main(void){
    float a;
    std::cout<< "Podaj długość boku: " <<std::endl;
    std::cin >> a;
    kwadrat kwadrat(a);
    std::cout << "Pole powierzchni kwadratu wynosi " << kwadrat.pole_powierzchni();
    
    return 0;
}
