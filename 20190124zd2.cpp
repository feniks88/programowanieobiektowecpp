#include <iostream>
class A
{
  private:
    char c;

  public:
    A(char);
    virtual void drukuj();
};

class B : public A
{
  private:
    int n;

  public:
    B(char, int);
    void drukuj();
};
A::A(char z)
{
    c = z;
}
void A::drukuj()
{
    std::cout << "c: " << c << std::endl;
}

B::B(char z1, int n1) : A(z1)
{
    n = n1;
}
void B::drukuj()
{
    A::drukuj();
    std::cout << "n: " << n << std::endl;
}

int main()
{
    A obiekt1('z');
    B obiekt2('5', 5);
    A *w = &obiekt1;
    w = &obiekt2;
    w->drukuj();

    return 0;
}