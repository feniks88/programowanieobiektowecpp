//
//  klasy2.cpp
//  
//
//  Created by macbook_monika on 11/10/2018.
//

/*
 
 UML
 
 klasa_tab
 
 - tab: int []
 
 + klasa_tab
 + drukuj(): void
 + szukaj(n: int) : bool
 
*/


#include <iostream>
#include <cassert>

class klasa_tab{ //deklaracja klasy, pól i metod
     
private:
	int tab[10];

public: // klasa pochodna ma dostęp do publicznej częsci klasy
    klasa_tab(int []);
    void drukuj();
    bool szukaj(int);
};

klasa_tab::klasa_tab(int t[]){ // definiowanie
	
	for(int i=0; i<9; i++){
        tab[i] = t[i];
        std::cout<<"konstruktor"<<std::endl;
    }
}

void klasa_tab::drukuj(){
    
    for(int i=0; i<9; i++){
        
        std::cout << tab[i] << " " << std::endl;
    }
}

bool klasa_tab::szukaj(int n){
    
    bool tmp = 0;
    for(int i = 0; i<9; i++){
        if(tab[i] == n){
            tmp = true;
            i = 9;
        }
    }
    return tmp;
}


int main(void){
    
    /*
     std::cout <<
     std::cin >>
     std::cout <<  << std::endl;
     */
    
    
	int tab[9] = {0,1,2,3,4,5,6,7,8};
    klasa_tab *w = new klasa_tab(tab);
    assert(w);
    w->drukuj();
	std::cout << w->szukaj(5); 
    
    delete w;
    
    return 0;
}

