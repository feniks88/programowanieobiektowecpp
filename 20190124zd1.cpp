#include <iostream>

template <class T>
T odejmowanie(T, T);

template <class Z>
class Nazwa
{
  private:
    Z element;

  public:
    Nazwa(Z);
    void wypisz();
    Z zwroc();
};
template <class Z>
Nazwa<Z>::Nazwa(Z zmienna)
{
    element = zmienna;
}
template <class Z>
void Nazwa<Z>::wypisz()
{
    std::cout << "Zmienna to: " << element << std::endl;
}

template <class Z>
Z Nazwa<Z>::zwroc()
{
    return element;
}

int main()
{
    //Szablony funkcji
    /*
    int l1, l2;
    float l3, l4;
    double l5, l6;

    std::cout << "Podaj dwie liczby typu int" << std::endl;
    std::cin >> l1 >> l2;
    std::cout << "Wynik to: " << odejmowanie(l1, l2) << std::endl;

    std::cout << "Podaj dwie liczby typu float" << std::endl;
    std::cin >> l3 >> l4;
    std::cout << "Wynik to: " << odejmowanie(l3, l4) << std::endl;

    std::cout << "Podaj dwie liczby typu dobule" << std::endl;
    std::cin >> l5 >> l6;
    std::cout << "Wynik to: " << odejmowanie(l5, l6) << std::endl;
    */
    //Szablony klass

    char c = 'z';
    Nazwa<char> znakowa(c);
    znakowa.wypisz();

    int a = 1;
    Nazwa<int> calkowite(a);
    calkowite.wypisz();

    float b = 1.23;
    Nazwa<float> zmienna(b);
    zmienna.wypisz();

    double d = 1.6788;
    Nazwa<double> zmiennaD(d);
    zmiennaD.wypisz();

       return 0;
}

template <class T>
T odejmowanie(T a, T b)
{
    return a - b;
}