#include <iostream>

class dwojka_liczb
{
  private:
    int l1, l2;

  public:
    dwojka_liczb();
    dwojka_liczb(int, int);
    int getL1();
    int getL2();
    virtual void wypisz();
};

class tensor : public dwojka_liczb
{
  private:
    int walencja;

  public:
    tensor(int);
    tensor(int, int, int);
    void wypisz();
    int slad();
};

dwojka_liczb::dwojka_liczb()
{
    l1 = 0;
    l2 = 0;
}
dwojka_liczb::dwojka_liczb(int a, int b)
{
    l1 = a;
    l2 = b;
}

int dwojka_liczb::getL1()
{
    return l1;
}
int dwojka_liczb::getL2()
{
    return l2;
}
2 tensor::tensor(int w) : dwojka_liczb()
{
    walencja = w;
}
tensor::tensor(int a, int b, int w) : dwojka_liczb(a, b)
{
    walencja = w;
}

void dwojka_liczb::wypisz()
{
    std::cout << "Liczba a: " << l1
              << ", Liczba b: " << l2 << std::endl;
}
void tensor::wypisz()
{
    dwojka_liczb::wypisz();
    std::cout << "Walencja: " << walencja << std::endl;
}

int tensor::slad()
{
    return (getL1() * getL2());
}

int main()
{
    int a, b, w;
    dwojka_liczb *wsk;
    std::cout << "Pierwsze dwie liczby: ";
    std::cin >> a >> b;

    dwojka_liczb obiekt1(a, b);

    std::cout << "Podaj drugie trzy libczy: ";
    std::cin >> a >> b >> w;

    tensor obiekt2(a, b, w);

    wsk = &obiekt1;
    wsk->wypisz();
    wsk = &obiekt2;
    wsk->wypisz();
    std::cout << "Slad: " << obiekt2.slad() << std::endl;
    return 0;
}