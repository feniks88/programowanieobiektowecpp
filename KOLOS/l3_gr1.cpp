#include <iostream>
#include <cstring>
class czasopismo
{
  private:
    char *tytul;
    static int ilosc_obiektow;

  public:
    czasopismo(char[]);
    ~czasopismo();
    czasopismo(const czasopismo &);
    void wypisz();
};

int czasopismo::ilosc_obiektow = 0;

czasopismo::czasopismo(char *napis)
{
    tytul = new char[strlen(napis) + 1];
    strcpy(tytul, napis);
    ilosc_obiektow++;
}

czasopismo::~czasopismo()
{
    delete[] tytul;
    ilosc_obiektow--;
}

czasopismo::czasopismo(const czasopismo &drugi)
{
    tytul = new char[strlen(drugi.tytul) + 1];
    strcpy(tytul, drugi.tytul);
    ilosc_obiektow++;
}

void czasopismo::wypisz()
{
    std::cout << "Ilosc obiektow: " << ilosc_obiektow << std::endl
              << "Napis: " << tytul << std::endl;
}

int main()
{
    czasopismo obiekt1("Tytul");
    std::cout << "Obiekt 1" << std::endl;
    obiekt1.wypisz();
    czasopismo obiekt2(obiekt1);
    std::cout << "Obiekt 2" << std::endl;
    obiekt2.wypisz();
    return 0;
}