// Kolos z programowania L3, grupa 2
// Prosze napisac program, w ktĂłrym jest klasa "dwojka_liczb", ktora ma 2 pola prywatne z liczbami calkowitymi.
// Przeciazyc konstruktor tak, aby po utworzeniu obiektu mozna go bylo zainicjalizowac wartosciami domyslnymi (0,0)
// lub wartosciami wpisywanymi z klawiatury na zadanie programu.
// Prosze zdefiniowac odpowiednia metode klasy, ktora wypisze na ekran zawartosc obiektu klasy.
// Z klasy "dwojka_liczb" prosze wyprowadzic w sposob publiczny klase "tensor2", ktora ma swoje wlasne pole calkowite
// o nazwie "walencja" i metode zwracajaca slad tensora

#include <iostream>

class dwojka_liczb {
private:
    int liczba1, liczba2;
public:
    dwojka_liczb();
    dwojka_liczb(int, int);
    int getL1();
    int getL2();
    virtual void wypisz();
};

class tensor2 : public dwojka_liczb {
private:
    int walencja;
public:
	tensor2(int);
	tensor2(int, int, int);
	void wypisz();
    int slad_tensora();
};

dwojka_liczb::dwojka_liczb() {
    liczba1 = 0;
    liczba2 = 0;
}

dwojka_liczb::dwojka_liczb(int _l1, int _l2) {
    liczba1 = _l1;
    liczba2 = _l2;
}

int dwojka_liczb::getL1() {
	return liczba1;
}

int dwojka_liczb::getL2() {
	return liczba2;
}

tensor2::tensor2(int w) : dwojka_liczb() {
	walencja = w;
}

tensor2::tensor2(int l1, int l2, int w) : dwojka_liczb(l1, l2) {
	walencja = w;
}

void dwojka_liczb::wypisz() {
    std::cout << "(" << liczba1 <<"; " << liczba2 << ")" << std::endl;
}

int tensor2::slad_tensora() {
//	nie wiadomo do konca jak ta funkcja ma dzialac, za malo info
    return (walencja * getL1()) + (walencja * getL2());
}

void tensor2::wypisz() {
	dwojka_liczb::wypisz();
	std::cout << "Walencja: " << walencja << std::endl;
}

int main() {
	int l1, l2, w;
    // TODO: uzupelnic main()
	dwojka_liczb * wskDwojka;
    std::cout << "Podaj dwie liczby calkowite: ";
    std::cin >> l1 >> l2;
    dwojka_liczb obiekt1(l1, l2);
    std::cout << "Podaj trzy liczby calkowite: ";
	std::cin >> l1 >> l2 >> w;
	tensor2 obiekt2(l1, l2, w);
	std::cout << "Obiekt 1:" << std::endl;
	
	wskDwojka = & obiekt1;
	wskDwojka->wypisz();
	
	std::cout << "Obiekt 2:" << std::endl;
	wskDwojka = & obiekt2;
	wskDwojka->wypisz();
	std::cout << "Slad tensora: " << obiekt2.slad_tensora() << std::endl;
    return 0;
}