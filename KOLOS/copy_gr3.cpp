#include <iostream>
#include <cstring>

class klasa
{
  private:
    int pole;

  public:
    klasa();
    klasa(int);
    friend std::ostream &operator<<(std::ostream &, klasa);
    friend std::istream &operator>>(std::istream &, klasa &);
};
klasa::klasa() {}
klasa::klasa(int a)
{
    pole = a;
}

std::ostream &operator<<(std::ostream &strumien, klasa obiekt)
{
    strumien << obiekt.pole;
    return strumien;
}
std::istream &operator>>(std::istream &strumien, klasa &obiekt)
{
    strumien >> obiekt.pole;
    return strumien;
}

int main()
{
    klasa obiekt1;
    std::cin >> obiekt1;
    std::cout << obiekt1;
    return 0;
}