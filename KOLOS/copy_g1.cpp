#include <iostream>
#include <cstring>
class czasopismo
{
  private:
    char *tytul;
    static int ilosc;

  public:
    czasopismo(char[]);
    ~czasopismo();
    czasopismo(const czasopismo &);
    void wypisz();
};
int czasopismo::ilosc = 0;

czasopismo::czasopismo(char *napis)
{
    tytul = new char[strlen(napis) + 1];
    strcpy(tytul, napis);
    ilosc++;
}
czasopismo::~czasopismo()
{
    delete[] tytul;
    if (ilosc > 0)
        ilosc--;
}

void czasopismo::wypisz()
{
    std::cout << "ilosc obietkow: " << ilosc << std::endl;
    std::cout << "Napis: " << tytul << std::endl;
}

czasopismo::czasopismo(const czasopismo &drugi)
{
    tytul = new char[strlen(drugi.tytul) + 1];
    strcpy(tytul, drugi.tytul);
    ilosc++;
}

int main()
{
    czasopismo obiekt1("Tytul");
    std::cout << "Obiekt1: ";
    obiekt1.wypisz();
    czasopismo obiekt2(obiekt1);
    std::cout << "Obiekt2: ";
    obiekt2.wypisz();
    return 0;
}