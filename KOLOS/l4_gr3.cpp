// w klasie:
class klasa
{
  private:
    int pole;
    // i inne pola
  public:
    // jakies metody, konstruktory, itd.
    friend std::ostream &operator<<(std::ostream &, klasa);
    friend std::istream &operator>>(std::istream &, klasa &);
};

// definicja funkcji przeciazajacej operator wypisania na strumien
std::ostream &operator<<(std::ostream &strumien, klasa obiekt)
{
    strumien << obiekt.pole;
    return strumien;
}

// definicja funkcji przeciazajacej operator wpisania ze strumienia
std::istream &operator>>(std::istream &strumien, klasa &obiekt)
{
    strumien >> obiekt.pole;
    return strumien;
}