//
//  main.cpp
//  mdabrowska08listop2018
//
//  Created by macbook_monika on 06/11/2018.
//  Copyright © 2018 macbook_monika. All rights reserved.
//

#include <iostream>

class dane
{
  private:
    int *tablica;
    int n;

  public:
    dane(int, int *);
    dane(const dane &tab);
    void wypisz();
    ~dane();
};
dane::dane(int size, int *tab)
{
    n = size;
    tablica = new int[size];
    for (int i = 0; i < n; i++)
    {
        tablica[i] = tab[i];
    }
}
void dane::wypisz()
{
    for (int i = 0; i < n; i++)
    {
        std::cout << tablica[i] << std::endl;
    }
}
dane::dane(const dane &tab)
{
    n = tab.n;
    tablica = new int[n];
    for (int i = 0; i < n; i++)
    {
        tablica[i] = tab.tablica[i];
    }
}
dane::~dane()
{
    delete[] tablica;
}

int main(int argc, const char *argv[])
{
    int tab[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int n = 10;
    dane tab1(n, tab);
    std::cout << "Tablica pierwsza: " << std::endl;
    tab1.wypisz();
    dane tab2(tab1);
    std::cout << "                              Tablica druga: " << std::endl;
    tab2.wypisz();

    return 0;
}
