#include <iostream>

void zamianaLiczbWsk(float *, float *);
void zamianaLiczbRef(float &, float &);

int main(void){
	
	float a;
	float b;
	
	std::cout << "Podaj liczbe a: " << std::endl;	
	std::cin >> a; 
	
	std::cout << "Podaj liczbe b: " << std::endl;
	std::cin >> b; 	
	
	zamianaLiczbWsk(&a, &b);
	std::cout << a << std::endl;
	std::cout << b << std::endl;
	
	std::cout << "REFERENCJA" << std::endl;
	
	std::cout << "Podaj liczbe a: " << std::endl;	
	std::cin >> a; 
	
	std::cout << "Podaj liczbe b: " << std::endl;
	std::cin >> b; 	
	zamianaLiczbRef(a,b);
	std::cout << a << std::endl;
	std::cout << b << std::endl;
	
	return 0;
} 

void zamianaLiczbRef(float &a, float &b){
	
	float tmp;
	if (a > b){
		tmp = a;
		a = b;
		b = tmp;
	}
	
}
void zamianaLiczbWsk(float *a, float *b){
	
	float tmp;
	if (*a > *b){
		tmp = *a;
		*a = *b;
		*b = tmp;
	}	
}
