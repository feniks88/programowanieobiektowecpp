#include <iostream>

#include <cstdlib>

template <class T>
T mnozenie(T, T);

int main()
{

    int a1, b1, iloczynInt;

    std::cout << "Prosze podac liczbe calkowita"
              << std::endl;
    std::cin >> a1;
    std::cout << "Prosze podac liczbe calkowita"
              << std::endl;
    std::cin >> b1;
    iloczynInt = mnozenie<int>(a1, b1);

    std::cout << "Wynik mnozenia dwoch liczb calkowitych: "
              << a1 << "*" << b1 << " wynosi: " << iloczynInt
              << std::endl;

    float a2, b2, iloczynFloat;

    std::cout << "Prosze podac liczbe zmiennopozycyjna"
              << std::endl;
    std::cin >> a2;
    std::cout << "Prosze podac liczbe zmiennopozycyjna"
              << std::endl;
    std::cin >> b2;
    iloczynFloat = mnozenie<float>(a2, b2);

    std::cout << "Wynik mnozenia dwoch liczb zmiennopozycyjnych "
              << a2 << "*" << b2 << " wynosi: " << iloczynFloat
              << std::endl;
    double a3, b3, iloczynDouble;

    std::cout << "Prosze podac liczbe zmiennopozycyjna podwojnej "
              << "precyzji" << std::endl;
    std::cin >> a3;
    std::cout << "Prosze podac liczbe zmiennopozycyjna podwojnej "
              << "precyzji" << std::endl;
    std::cin >> b3;
    iloczynDouble = mnozenie<double>(a3, b3);

    std::cout << "Wynik mnozenia dwoch liczb zmiennopozycyjnych "
              << "podwojnej precyzji " << a3 << "*" << b3
              << "wynosi: " << iloczynDouble << std::endl;
    return 0;
}

template <class T>
T mnozenie(T x, T y)
{
    return x * y;
}
