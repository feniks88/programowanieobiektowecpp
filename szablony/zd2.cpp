#include <iostream>
#include <string>
int main()
{
std::string pierwszy("To jest test."); // inicjalizacja ciagiem znakow NBTS (na ktory
// wskazuje wskaznik "w"), przy pomocy konstruktora 1: string(const char * w)

std::cout << "pierwszy zawiera: " << pierwszy << std::endl; // dziala przeciazony
//operator "<<" wypisania na strumien

std::string drugi(20, '$'); //tworzenie obiektu o dlugosci 20 el., wypelnionych znakami '$',
// przy pomocy konstruktora 2: string(size_type m, char c)

std::cout << "drugi zawiera: " << drugi << std::endl;
std::string trzeci(pierwszy); // inicjalizacja obiektu innym obiektem,
// przy pomocy konstruktora 3 (kopiujacego): string(const string &)

std::cout << "trzeci zawiera: " << trzeci << std::endl;
pierwszy += "Halo !"; // dziala przeciazony operator "+="
std::cout << "teraz pierwszy zawiera: " << pierwszy << std::endl;
drugi = "Teraz w drugim: jeszcze raz test !"; // przypisanie lancucha
trzeci[10] = 'n'; // dziala przeciazony operator indeksacji "[ ]"
std::string czwarty ; // kosntruktor 4: bezparametrowy
czwarty = drugi + trzeci; // dzialaja przeciazone operatory "+" i "="
std::cout << "czwarty zawiera: " << czwarty << std::endl;
char tab[ ] = "Tekst z tablicy \"tab\" ";
std::string piaty(tab, 22); // inicjalizacja poczatkowymi 22 znakami z lancucha
// zawartego w tablicy "tab",
// przy pomocy konstruktora 5: string(const char* w, size_type m)
std::cout << "piaty zawiera: " << piaty << std::endl;
std::string szosty(tab + 9, tab + 14); // ten konstruktor (nr 6) ma argument w postaci
// szablonu: template<class itr> string(itr begin, itr end),
// gdzie "begin" i "end" dzialaja, jak wskazniki
// do inicjalizacji sa uzyte wartosci zawarte w zakresie [begin, end),
// czyli w przedziale lewostronnie domknietym, "end" wskazuje
//bowiem na miejsce ZA ostatnim elementem
std::cout << "szosty zawiera: " << szosty << std::endl;
std::string siodmy(&piaty[9], &piaty[14]); // jeszcze raz konstruktor nr 6
std::cout << "siodmy zawiera: " << siodmy << std::endl;
std::string osmy(czwarty, 20, 9); // konstruktor 7 kopiuje fragment ciagu znakow z
// obiektu "czwarty",
// do obiektu "osmy", tutaj jest kopiowanych 9 znakow, od pozycji
// 20, czyli od znaku 21
std::cout << "osmy zawiera: " << osmy << std::endl;
return 0;
}