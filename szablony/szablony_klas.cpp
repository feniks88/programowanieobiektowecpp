#include <iostream>
#include <cstdlib>

class punkt
{
    int X, Y;

  public:
    punkt();
    punkt(const punkt &);
    void ustaw(int, int);
    friend std::ostream &operator<<(std::ostream &, const punkt &);
};

punkt::punkt()
{
}

punkt::punkt(const punkt &p)
{
    X = p.X;
    Y = p.Y;
}

void punkt::ustaw(int a, int b)
{
    X = a;
    Y = b;
}

std::ostream &operator<<(std::ostream &wyjscie, const punkt &p)
{
    wyjscie << "(" << p.X << ", " << p.Y << ")";
    return wyjscie;
}

template <class T>
class stos
{
    T *elementy;
    int wierzch, rozmiar;

  public:
    stos(int);
    void poloz(const T &); //angielska nazwa: "push( )"
    T zdejmij();           // angielska nazwa: "pop( )"
    bool czyPusty() const; // sprawdzanie, czy stos jest pusty
    ~stos();
};

template <class T>
stos<T>::stos(int n = 100) : rozmiar(n), wierzch(0)
{
    elementy = new T[n];
}

template <class T>
void stos<T>::poloz(const T &z)
{
    if (wierzch < rozmiar)
        elementy[wierzch++] = z;

    else
    {
        T *ptr = new T[rozmiar * 2];

        for (int i = 0; i < rozmiar; i++)
            ptr[i] = elementy[i];

        delete[] elementy;

        elementy = ptr;

        rozmiar *= 2;

        std::cout << "Nowy rozmiar: " << rozmiar << std::endl;

        elementy[wierzch++] = z;
    }
}

template <class T>
T stos<T>::zdejmij()
{
    return elementy[--wierzch];
}

template <class T>
bool stos<T>::czyPusty() const // uwaga na "const"
{
    return wierzch == 0;
}

template <class T>
stos<T>::~stos()
{
    delete[] elementy;
}

int main()
{

    punkt dane[5]; // Co to jest ????

    dane[0].ustaw(0, 1);
    dane[1].ustaw(1, 2);
    dane[2].ustaw(-4, 5);
    dane[3].ustaw(20, 0);
    dane[4].ustaw(45, 28);

    try
    {

        stos<punkt> obiektStos(4);

        int n = sizeof(dane) / sizeof(punkt);

        std::cout << "na poczatku mamy dane: " << std::endl;

        for (int k = 0; k < n; k++)
            std::cout << dane[k] << " ";

        std::cout << std::endl;

        for (int m = 0; m < n; m++)
            obiektStos.poloz(dane[m]);

        std::cout << "Zdejmowanie danych ze stosu, "
                  << "az stanie sie pusty." << std::endl;

        while (!obiektStos.czyPusty())
            std::cout << obiektStos.zdejmij() << " ";

        std::cout << std::endl;
    }

    catch (std::bad_alloc wyjatek)
    {
        wyjatek.what();
    }

    return 0;
}