#include <iostream>

class A
{
    char c;

  public:
    A(char);
    char zwroc();
    virtual void drukuj();
};

class B : public A
{
    int n;

  public:
    B(int, char);
    void drukuj();
};
A::A(char x)
{
    c = x;
}
char A::zwroc()
{
    return c;
}

void A::drukuj()
{
    std::cout << "A= " << c << std::endl;
}

B::B(int a, char b) : A(b)
{
    n = a;
}

void B::drukuj()
{
    std::cout << "A= " << zwroc() << std::endl;
    std::cout << "B= " << n << std::endl;
}

int main()
{
    int a;
    char b, c;

    std::cout << "Podaj liczbę a: " << std::endl;
    std::cin >> a;
    std::cout << "Podaj literę pierwsza: " << std::endl;
    std::cin >> b;
    std::cout << "Podaj literę druga: " << std::endl;
    std::cin >> c;

    A obiekt1(c);
    B obiekt2(a, b);
    obiekt1.drukuj();
    obiekt2.drukuj();

    return 0;
}