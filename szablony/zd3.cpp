#include <iostream>
#include <string>

class Zamek
{
    char *nazwa;

  public:
    static Zamek *wsk_siedziby;
    Zamek(char *);
    void opisz();
    static void gdzie_krol();
};

Zamek *Zamek::wsk_siedziby;
Zamek::Zamek(char *n) : nazwa(n) {}
void Zamek::opisz()
{
    std::cout << "tutaj" << nazwa << std::endl;
};

void Zamek::gdzie_krol()
{
    std::cout << "komunikat do funkcji statycznej: "
              << "krol jest teraz w zamku: " << wsk_siedziby->nazwa;
}

template <class typ>
class castle
{
    char *nazwa;

  public:
    static castle<typ> *wsk_castle;
    static int calkowity;
    static typ skladnik_t;
    castle(char *);
    void opisz();
    static void gdzie_stolica();
};

template <class typ>
castle<typ> *castle<typ>::wsk_castle;

template <class typ>
int castle<typ>::calkowity;

template <class typ>
typ castle<typ>::skladnik_t;

template <class typ>
castle<typ>::castle(char *n) : nazwa(){};

template <class typ>
void castle<typ>::opisz()
{
    std::cout << "tutaj: " << nazwa << std::endl;
}

template <class typ>
void castle<typ>::gdzie_stolica()
{
    std::cout << "komunikat od funkcji statycznej: "
              << "stolica: " << wsk_castle->nazwa;
}

int main()
{
    Zamek zamekWawelski("Wawel");
    Zamek zamekNiepolomicki("Niepolomice");

    Zamek::wsk_siedziby = &zamekWawelski;

    zamekWawelski.opisz();
    zamekNiepolomicki.opisz();

    Zamek::gdzie_krol();
    std::cout << "krol wyjechal do niepolomic" << std::endl;

    zamekWawelski.wsk_siedziby = &zamekNiepolomicki;

    zamekWawelski.opisz();
    zamekNiepolomicki.opisz();

    Zamek::gdzie_krol();

    castle<char> zamekWindsor("Windsor");
    castle<char> zamekTower("Tower");

    castle<char>::calkowity = 50;
    castle<char>::skladnik_t = 'y';
    castle<char>::wsk_castle = &zamekTower;

    zamekWindsor.opisz();
    zamekTower.opisz();

    castle<char>::gdzie_stolica();

    castle<int> zamekKyoto("Kyoto");
    castle<int> zamekTokio("Tokio");
    castle<int>::calkowity = 4;
    castle<int>::skladnik_t = 'h';
    castle<int>::wsk_castle = &zamekKyoto;

    zamekKyoto.opisz();
    zamekTokio.opisz();

    castle<int>::gdzie_stolica();

    return 0;
}