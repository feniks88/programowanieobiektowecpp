#include <iostream>

template <class T>
T zlicz(T, T[10]);

int main()
{

    float tab1[10];
    float a;

    std::cout << "Prosze podac 10 liczb typu float: " << std::endl;

    for (int i = 0; i < 10; i++)
    {
        std::cin >> tab1[i];
    }

    std::cout << "Prosze podac wyszukiwana liczne: " << std::endl;
    std::cin >> a;

    int odp = zlicz<float>(a, tab1);

    std::cout << "Liczba wystepuje w tablicy: " << odp << std::endl;

    double tab2[10];
    double b;

    std::cout << "Prosze podac 10 liczb typu double: " << std::endl;

    for (int i = 0; i < 10; i++)
    {
        std::cin >> tab2[i];
    }

    std::cout << "Prosze podac wyszukiwana liczne: " << std::endl;
    std::cin >> b;

    odp = zlicz<double>(b, tab2);

    std::cout << "Liczba wystepuje w tablicy: " << odp << std::endl;
}

template <class T>
T zlicz(T liczba, T tablica)
{
    int wystopienia = 0;
    for (int i = 0; i < 10; i++)
    {
        if (tablica[i] == liczba)
            wystopienia++;
    }
    return wystopienia;
}
