#include <iostream>
#include <cassert>
#include <cstring>

void memory_error( )
{
    std::cerr << "Alokacja nie powiodla się." << std::endl;
    exit(1);
}

class srodek_transportu
{
private:
    char *model;
    
public:
    srodek_transportu(char *);
    virtual void drukuj();
    char *zwrocModel();
    ~srodek_transportu();
};

srodek_transportu::srodek_transportu(char *napis)
{
    int dlugosc_napisu = strlen(napis);
    model = new char[dlugosc_napisu + 1];
    std::set_new_handler(memory_error);
    std::strcpy(model, napis);
}
char * srodek_transportu::zwrocModel(){
    return model;
}

srodek_transportu::~srodek_transportu()
{
    std::cout << "Usunieto srodek transportu" << std::endl;
    delete[] model;
}

void srodek_transportu::drukuj()
{
    std::cout << "Srodek transportu typ to: " << model << std::endl;
}

class samochod : public srodek_transportu
{
private:
    char *paliwo;
    
public:
    samochod(char *,char *);
    void drukuj();
    ~samochod();
};
samochod::samochod(char *typ_paliwa, char *typ_samochodu) : srodek_transportu(typ_samochodu)
{
    paliwo = new char[strlen(typ_paliwa) + 1];
    std::set_new_handler(memory_error);
    strcpy(paliwo, typ_paliwa);
}

void samochod::drukuj()
{
    std::cout << "Typ samochodu to: " << zwrocModel() << std::endl;
    std::cout << "Paliwo w samochodzie to: " << paliwo << std::endl;
}

samochod::~samochod()
{
    std::cout << "Usunieto samochod" << std::endl;
    delete[] paliwo;
}



int main()
{
    char *tekst1 = "obiekt1";
    srodek_transportu obiekt1(tekst1);
    obiekt1.drukuj();
    char *tekst2 = "obiekt2";
    char *tekst3 = "paliwo1";
    samochod obiekt2(tekst3,tekst2);
    obiekt2.drukuj();
}
